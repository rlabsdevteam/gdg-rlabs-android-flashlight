package org.rlabs.gdg.flashlight;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;

public class Main extends Activity implements OnClickListener {
	// private variable is a safe place to keep data while you application is
	// running.
	ToggleButton toggleButton1 = null;
	Camera camera = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Info: At a start of any Android Application is the 'OnCreate'
		// function. This is where apps begins their execution.
		super.onCreate(savedInstanceState);
		// You can to let it load a view
		setContentView(R.layout.main);
		// Info: We create our first function to do more code for us
		setupView();
	}

	private void setupView() {
		// Info: The code will continue from here
		// This Toggle button links what is on your screen, below the toggle
		// button you see
		// with something called a Listener.
		toggleButton1 = (ToggleButton) findViewById(R.id.toggleButton1);
		// A listener is a class in java that wait for something to happens, but
		// you got to ask it to listen to something first.

		toggleButton1.setOnClickListener(this);
		// Since we using feature of the camera we need to open it for the start
		// to begin asking details, and manipulating the flash feature later on
		// in our application.
		camera = Camera.open();
	}

	@Override
	public void onClick(View v) {
		// Below we need to determine whether we first got to turn on the
		// camera's flash or turn it off in case we done it before. To check if
		// its on, we must assume the toggle button is set to be on. So we use a
		// function within the toggle button called 'isChecked'.
		if (toggleButton1.isChecked()) {
			// This code below will only run if the toggle clicked to be on.
			Parameters p = camera.getParameters();
			p.setFlashMode(Parameters.FLASH_MODE_TORCH);
			camera.setParameters(p);
			camera.startPreview();
		} else {
			// This code below will only run if the toggle clicked to be off.
			Parameters p = camera.getParameters();
			p.setFlashMode(Parameters.FLASH_MODE_OFF);
			camera.setParameters(p);
			camera.stopPreview();
		}

	}
}
